/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.cottacush;

import com.silverbullet.gaora.responses.BaseResponse;
import com.silverbullet.gaora.responses.ResponseUtil;
import java.util.List;
import java.util.Objects;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author prodigy4440
 */
@Stateless
public class Service {

    @PersistenceContext
    private EntityManager entityManager;

    public BaseResponse fetchAll(Long lastID) {
        if (Objects.isNull(lastID) || (lastID < 0)) {
            lastID = 0L;
        }

        List<Stock> stocks = entityManager.createNamedQuery("Stock.findAll", Stock.class)
                .setParameter("id", lastID).setMaxResults(50).getResultList();
        return ResponseUtil.success("Stocks", stocks);
    }
    
    public BaseResponse averageVolumePerMonth(Integer year){
        List list = entityManager.createNamedQuery("Stock.findAverageVolumePerMonths")
                .setParameter("year", year).getResultList();
        return ResponseUtil.success("Successs", list);
    }
    
    public BaseResponse minAndMaxPerMonth(Integer year){
        List list = entityManager.createNamedQuery("Stock.findAverageHighLow")
                .setParameter("year", year).getResultList();
        return ResponseUtil.success("Success", list);
    }
    
    public BaseResponse monthlyVolumeTotal(Integer year){
        List list = entityManager.createNamedQuery("Stock.findMonthlyTotal")
                .setParameter("year", year).getResultList();
        return ResponseUtil.success("Success", list);
    }
    
    public BaseResponse monthlyVolumeAverage(Integer year){
        List list = entityManager.createNamedQuery("Stock.findMonthlyAverage")
                .setParameter("year", year).getResultList();
        return ResponseUtil.success("Success", list);
    }
}
