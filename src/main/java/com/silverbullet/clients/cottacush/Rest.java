/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.cottacush;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author prodigy4440
 */
@Stateless
@Path("rest")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class Rest {

    @EJB
    private Service service;

    @Path("all")
    @GET
    public Response fetchAll(@QueryParam("lastid") Long id) {
        return Response.ok(service.fetchAll(id)).build();
    }
    
    @Path("average/volume/per/month")
    @GET
    public Response averageVolumePerMonth(@QueryParam("year")Integer year){
        return Response.ok(service.averageVolumePerMonth(year)).build();
    }
    
    @Path("minmax/per/month")
    @GET
    public Response monthlyMinMax(@QueryParam("year")Integer year){
        return Response.ok(service.minAndMaxPerMonth(year)).build();
    }
    
    @Path("monthly/volume/total")
    @GET
    public Response monthlyVolumeTotal(@QueryParam("year")Integer year){
        return Response.ok(service.monthlyVolumeTotal(year)).build();
    }
        
    @Path("monthly/volume/average")
    @GET
    public Response monthlyVolumeAverage(@QueryParam("year")Integer year){
        return Response.ok(service.monthlyVolumeAverage(year)).build();
    }
}
