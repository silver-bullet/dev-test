/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.cottacush;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author prodigy4440
 */
@NamedQueries({
    @NamedQuery(
            name = "Stock.findAll",
            query = "SELECT s FROM Stock s WHERE s.id > :id ORDER BY s.id ASC"),
    @NamedQuery(name = "Stock.findAverageVolumePerMonths",
            query = "SELECT MONTH(s.date), AVG(s.stockAdjustedClose) FROM Stock s "
                    + "WHERE YEAR(s.date) = :year GROUP BY MONTH(s.date)"),
    @NamedQuery(name = "Stock.findAverageHighLow",
            query = "SELECT MONTH(s.date), MIN(s.stockLow), MAX(s.stockHigh) FROM "
                    + "Stock s WHERE YEAR(s.date) = :year GROUP BY MONTH(s.date)"),
    @NamedQuery(name = "Stock.findMonthlyTotal",
            query = "SELECT MONTH(s.date), SUM(s.stockVolume) FROM Stock s WHERE "
                    + "YEAR(s.date) = :year GROUP BY MONTH(s.date)"),
    @NamedQuery(name = "Stock.findMonthlyAverage",query = "SELECT MONTH(s.date)"
            + ", AVG(s.stockVolume) FROM Stock s WHERE YEAR(s.date) = :year "
            + "GROUP BY MONTH(s.date)")
})
@Entity
@Table(name = "stock")
public class Stock implements Serializable{
    
    private static final long serialVersionUID = 5175583305623167894L;
    
    @Id
    @Column(name = "id", nullable = false,unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "stock_date")
    @Temporal(TemporalType.DATE)
    private Date date;
    
    @Column(name = "stock_open")
    private Double stockOpen;
    
    @Column(name = "stock_high")
    private Double stockHigh;
    
    @Column(name = "stock_low")
    private Double stockLow;
    
    @Column(name = "stock_close")
    private Double stockClose;
    
    @Column(name = "stock_volume")
    private Integer stockVolume;
    
    @Column(name = "stock_adjusted_close")
    private Double stockAdjustedClose;

    public Stock() {
    }

    public Stock(Date date, Double stockOpen, Double stockHigh, Double stockLow, 
            Double stockClose, Integer stockVolume, Double stockAdjustedClose) {
        this.date = date;
        this.stockOpen = stockOpen;
        this.stockHigh = stockHigh;
        this.stockLow = stockLow;
        this.stockClose = stockClose;
        this.stockVolume = stockVolume;
        this.stockAdjustedClose = stockAdjustedClose;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getStockOpen() {
        return stockOpen;
    }

    public void setStockOpen(Double stockOpen) {
        this.stockOpen = stockOpen;
    }

    public Double getStockHigh() {
        return stockHigh;
    }

    public void setStockHigh(Double stockHigh) {
        this.stockHigh = stockHigh;
    }

    public Double getStockLow() {
        return stockLow;
    }

    public void setStockLow(Double stockLow) {
        this.stockLow = stockLow;
    }

    public Double getStockClose() {
        return stockClose;
    }

    public void setStockClose(Double stockClose) {
        this.stockClose = stockClose;
    }

    public Integer getStockVolume() {
        return stockVolume;
    }

    public void setStockVolume(Integer stockVolume) {
        this.stockVolume = stockVolume;
    }

    public Double getStockAdjustedClose() {
        return stockAdjustedClose;
    }

    public void setStockAdjustedClose(Double stockAdjustedClose) {
        this.stockAdjustedClose = stockAdjustedClose;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.id);
        hash = 37 * hash + Objects.hashCode(this.date);
        hash = 37 * hash + Objects.hashCode(this.stockOpen);
        hash = 37 * hash + Objects.hashCode(this.stockHigh);
        hash = 37 * hash + Objects.hashCode(this.stockLow);
        hash = 37 * hash + Objects.hashCode(this.stockClose);
        hash = 37 * hash + Objects.hashCode(this.stockVolume);
        hash = 37 * hash + Objects.hashCode(this.stockAdjustedClose);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Stock other = (Stock) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        if (!Objects.equals(this.stockOpen, other.stockOpen)) {
            return false;
        }
        if (!Objects.equals(this.stockHigh, other.stockHigh)) {
            return false;
        }
        if (!Objects.equals(this.stockLow, other.stockLow)) {
            return false;
        }
        if (!Objects.equals(this.stockClose, other.stockClose)) {
            return false;
        }
        if (!Objects.equals(this.stockVolume, other.stockVolume)) {
            return false;
        }
        if (!Objects.equals(this.stockAdjustedClose, other.stockAdjustedClose)) {
            return false;
        }
        return true;
    }

    
}
