/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.cottacush;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.ParseException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author prodigy4440
 */
public class Snippet {

    public static void main(String[] args) throws IOException,
            ParseException {
        List<String> lines = Files.readAllLines(
                Paths.get("/home/prodigy4440/Documents/NetBeansProjects/CottaCush/src/main/java/com/silverbullet/clients/cottacush/rawDataForTest.csv"));

        String sql = "INSERT INTO stock (id, stock_date, stock_open, stock_high, stock_low, stock_close,stock_volume, stock_adjusted_close) VALUES ";
        
        List<String> rows = new LinkedList<>();
        
        int count = 0;
        for (String line : lines) {
            if(count !=0){
                String[] split = line.split(",");
                List<String> asList = Arrays.asList(split[0].split("/"));
                String date = "20"+asList.get(2)+"/"+asList.get(1)+"/"+asList.get(0);
                String open = split[1];
                String high = split[2];
                String low = split[3];
                String close = split[4];
                String volume = split[5];
                String adjustedClose = split[6];
                
                 String row = "(NULL,"+"\""+date+"\""+","+"\""+open+"\""+","+"\""+high+"\""+","+"\""+low+"\""+","+"\""+close+"\""+","+"\""+volume+"\""+","+"\""+adjustedClose+"\""+")"+"\n";
                 rows.add(row);
            }
           count++;
        }
        
        sql+=String.join(",", rows)+";";
        BufferedWriter bufferedWriter = Files.newBufferedWriter(Paths
                .get("/home/prodigy4440/Desktop/cotta.sql"), 
                StandardOpenOption.CREATE_NEW, StandardOpenOption.APPEND);
        bufferedWriter.append(sql);
        bufferedWriter.flush();
        bufferedWriter.close();
    }
}
