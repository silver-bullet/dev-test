/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var app = angular.module("cotta", ["chart.js"]);

app.controller("StockController", function ($scope, $log, $http) {

    $scope.stocks = {};

    $scope.avglabels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul'
                , 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    $scope.avgseries = ['Average Adjusted Close'];
    $scope.minmaxseries = ['Min','Max'];
    $scope.avgdata = [
        []
    ];
    $scope.totalvolumedata = [];
    $scope.averagevolumedata = [
        []
    ];
    $scope.minmaxdata = [
        [], []
    ];



    $scope.loadStocks = function () {

        $scope.allstocksurl = "http://localhost:8080/cottacush/api/v1.0/rest/all";

        $scope.config = {};
        $scope.config.params = {};
        $scope.config.headers = {
            'Content-Type': "application/json"
        };
        $scope.config.params.lastid = 0;
        $http.get($scope.allstocksurl, $scope.config).then(function (response) {
            $scope.stocks = response.data.data;
        }, function (response) {
            $log.warn(response);
        });
    };

    $scope.loadAverageAdjClose = function () {

        $scope.allstocksurl =
                "http://localhost:8080/cottacush/api/v1.0/rest/average/volume/per/month";

        $scope.config = {};
        $scope.config.params = {};
        $scope.config.headers = {
            'Content-Type': "application/json"
        };
        $scope.config.params.year = 2016;
        $http.get($scope.allstocksurl, $scope.config).then(function (response) {
            angular.forEach(response.data.data, function (value, key) {
                value[0];
                $scope.avgdata[0].push(value[1]);
            });
            $log.info($scope.avgdata);
        }, function (response) {
            $log.warn(response);
        });
    };

    $scope.loadMinMax = function () {

        $scope.allstocksurl =
                "http://localhost:8080/cottacush/api/v1.0/rest/minmax/per/month";

        $scope.config = {};
        $scope.config.params = {};
        $scope.config.headers = {
            'Content-Type': "application/json"
        };
        $scope.config.params.year = 2016;
        $http.get($scope.allstocksurl, $scope.config).then(function (response) {
            angular.forEach(response.data.data, function (value, key) {
                value[0];
                $scope.minmaxdata[0].push(value[1]);
                $scope.minmaxdata[1].push(value[2]);
            });
            $log.info($scope.avgdata);
        }, function (response) {
            $log.warn(response);
        });
    };

    $scope.loadMonthlyTotalVolume = function () {

        $scope.allstocksurl =
                "http://localhost:8080/cottacush/api/v1.0/rest/monthly/volume/total";

        $scope.config = {};
        $scope.config.params = {};
        $scope.config.headers = {
            'Content-Type': "application/json"
        };
        $scope.config.params.year = 2016;
        $http.get($scope.allstocksurl, $scope.config).then(function (response) {
            angular.forEach(response.data.data, function (value, key) {
                value[0];
                $scope.totalvolumedata.push(value[1]);
            });
        }, function (response) {
            $log.warn(response);
        });
    };

    $scope.loadMonthlyAverageVolume = function () {

        $scope.allstocksurl =
                "http://localhost:8080/cottacush/api/v1.0/rest/monthly/volume/average";

        $scope.config = {};
        $scope.config.params = {};
        $scope.config.headers = {
            'Content-Type': "application/json"
        };
        $scope.config.params.year = 2016;
        $http.get($scope.allstocksurl, $scope.config).then(function (response) {
            angular.forEach(response.data.data, function (value, key) {
                value[0];
                $scope.averagevolumedata[0].push(value[1]);
            });
        }, function (response) {
            $log.warn(response);
        });
    };

    $scope.loadStocks();
    $scope.loadAverageAdjClose();
    $scope.loadMinMax();
    $scope.loadMonthlyTotalVolume();
    $scope.loadMonthlyAverageVolume();

});
